locals {

  region       = get_env("REGION", get_env("AWS_DEFAULT_REGION", "eu-central-1")) 
  account_id   = get_aws_account_id()

  region_hash  = substr(sha256(local.region), 0, 5)
  account_hash = substr(sha256(local.account_id), 0, 5)

  department      = get_env("DEPARTMENT", get_env("CI_PROJECT_ROOT_NAMESPACE", "DEPARTMENT_NOT_PROVIDED"))
  stage           = get_env("STAGE",      get_env("CI_COMMIT_REF_NAME", "STAGE_NOT_PROVIDED"))
  service         = get_env("SERVICE",    get_env("CI_PROJECT_NAME", "PROJECT_NOT_PROVIDED"))
  service_version = get_env("VERSION",    get_env("CI_COMMIT_SHORT_SHA", "VERSION_NOT_PROVIDED"))

  git_project  = get_env("CI_PROJECT_URL", "CI_PROJECT_URL_NOT_PROVIDED")
  git_user     = get_env("GITLAB_USER_NAME", "GITLAB_USER_NAME_NOT_PROVIDED")
  pipeline_url = get_env("CI_PIPELINE_URL", "CI_PIPELINE_URL_NOT_PROVIDED")

  TERRAFORM_PROVIDER_AWS = get_env("TERRAFORM_PROVIDER_AWS")
  TERRAFORM_PROVIDER_GITHUB = get_env("TERRAFORM_PROVIDER_GITHUB")
  TERRAFORM_PROVIDER_KUBERNETES = get_env("TERRAFORM_PROVIDER_KUBERNETES")
  TERRAFORM_PROVIDER_ARCHIVE = get_env("TERRAFORM_PROVIDER_ARCHIVE")
  TERRAFORM_PROVIDER_TEMPLATE = get_env("TERRAFORM_PROVIDER_TEMPLATE")

  timestamp = formatdate("YYYY_MM_DD_hh_mm", timestamp())
}

inputs = {

}

# ########################################################################################################################
#
# GENERATE REMOTE-STATE FILE
#
# ########################################################################################################################

remote_state {
  backend = "s3"
  generate = {
    path      = "00_generated_00_backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = join("-", [ local.account_hash, local.region_hash, "tf", "states"])
    key    = join("/", [ local.department, local.stage, local.service, "terraform.tfstate"])
    region = "${local.region}"
    encrypt = true
  }
}

# ########################################################################################################################
#
# GENERATE PROVIDER FILE
#
# ########################################################################################################################

generate "provider" {
  path = "00_generated_01_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "${local.TERRAFORM_PROVIDER_AWS}"
    }

    github = {
      source  = "integrations/github"
      version = "${local.TERRAFORM_PROVIDER_GITHUB}"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "${local.TERRAFORM_PROVIDER_KUBERNETES}"
    }

    archive = {
      source = "hashicorp/archive"
      version = "${local.TERRAFORM_PROVIDER_ARCHIVE}"
    }

    template = {
      source = "hashicorp/template"
      version = "${local.TERRAFORM_PROVIDER_TEMPLATE}"
    }
  }
}

provider "aws" {
  region = "${local.region}"
}

provider "aws" {
  alias  = "us_east_1"
  region = "us-east-1"
}

EOF
}

# ########################################################################################################################
#
# GENERATE LOCALS FILE
#
# ########################################################################################################################

generate "locals" {
  path = "00_generated_03_locals.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
locals {

  department   = "${local.department}"
  stage        = "${local.stage}"
  service      = "${local.service}"

  account      = "${local.account_id}"
  account_hash = "${local.account_hash}"

  region       = "${local.region}"
  region_hash  = "${local.region_hash}"

  stage_prefix = join("-", [local.account_hash, local.region_hash, local.department, local.stage])
  service_prefix = join("-", [local.account_hash, local.region_hash, local.department, local.stage, local.service])

  default_tags = {
    department           = local.department
    stage                = local.stage
    service              = local.service
    service_version      = "${local.service_version}"
    project_source       = "${local.git_project}"
    deployment_timestamp = "${local.timestamp}"
    deployment_pipeline  = "${local.pipeline_url}"
    deployment_user      = "${local.git_user}"
  }
}
EOF
}

