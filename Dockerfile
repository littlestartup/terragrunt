FROM alpine:3.19.0@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48

# renovate: datasource=terraform-provider depName=aws
ENV TERRAFORM_PROVIDER_AWS_VERSION=5.31.0

# renovate: datasource=terraform-provider depName=github
ENV TERRAFORM_PROVIDER_GITHUB_VERSION=5.42.0

# renovate: datasource=terraform-provider depName=kubernetes
ENV TERRAFORM_PROVIDER_KUBERNETES_VERSION=2.24.0

# renovate: datasource=terraform-provider depName=archive
ENV TERRAFORM_PROVIDER_ARCHIVE_VERSION=2.4.1

# renovate: datasource=terraform-provider depName=template
ENV TERRAFORM_PROVIDER_TEMPLATE_VERSION=2.2.0

# renovate: datasource=github-releases depName=gruntwork-io/terragrunt extractVersion=^v(?<version>.*)$
ENV TERRAGRUNT_VERSION=0.54.7

# renovate: datasource=github-releases depName=hashicorp/terraform extractVersion=^v(?<version>.*)$
ENV TERRAFORM_VERSION=1.6.6

# renovate: datasource=github-releases depName=terraform-linters/tflint extractVersion=^v(?<version>.*)$
ENV TFLINT_VERSION=0.49.0

# renovate: datasource=github-releases depName=terraform-linters/tflint-ruleset-aws extractVersion=^v(?<version>.*)$
ENV TFLINT_RULESET_AWS_VERSION=0.28.0

ENV TF_PLUGIN_CACHE_DIR=/.terraform

ARG TERRAFORM_TMPDIR=/.tmp/terraform
ARG ROOT_DIR=/root
ARG ARCH=arm64

RUN apk add --no-cache --update groff tree unzip bash git openssh curl python3 py3-pip aws-cli

ADD https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_${ARCH} /usr/local/bin/terragrunt
RUN chmod +x /usr/local/bin/terragrunt

ADD https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_${ARCH}.zip terraform.zip 
RUN unzip terraform.zip && \
    rm terraform.zip && \
    chmod +x terraform && \
    mv terraform /usr/local/bin/terraform

COPY terraform ${TERRAFORM_TMPDIR}

RUN cd ${TERRAFORM_TMPDIR} && \
    terragrunt init && \
    cd ${ROOT_DIR} && \
    rm -rf ${TERRAFORM_TMPDIR}

COPY terragrunt /terragrunt
ENV TERRAGRUNT_BASE=/terragrunt/terragrunt.hcl

COPY bin /usr/local/bin
RUN chmod +x /usr/local/bin/*

ADD https://github.com/terraform-linters/tflint/releases/download/v${TFLINT_VERSION}/tflint_linux_${ARCH}.zip tflint.zip
RUN unzip tflint.zip && \
    rm tflint.zip && \
    chmod +x tflint && \
    mv tflint /usr/local/bin/tflint && \
    mkdir -p ${ROOT_DIR}/.tflint.d/plugins
ADD https://github.com/terraform-linters/tflint-ruleset-aws/releases/download/v$TFLINT_RULESET_AWS_VERSION/tflint-ruleset-aws_linux_${ARCH}.zip ruleset.zip
RUN unzip ruleset.zip -d ${ROOT_DIR}/.tflint.d/plugins && \
    rm ruleset.zip
COPY tflint/tflint.hcl ${ROOT_DIR}/.tflint.hcl

CMD ['/bin/bash']
