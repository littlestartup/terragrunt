locals {
  TERRAFORM_PROVIDER_AWS_VERSION = get_env("TERRAFORM_PROVIDER_AWS_VERSION")
  TERRAFORM_PROVIDER_GITHUB_VERSION = get_env("TERRAFORM_PROVIDER_GITHUB_VERSION")
  TERRAFORM_PROVIDER_KUBERNETES_VERSION = get_env("TERRAFORM_PROVIDER_KUBERNETES_VERSION")
  TERRAFORM_PROVIDER_ARCHIVE_VERSION = get_env("TERRAFORM_PROVIDER_ARCHIVE_VERSION")
  TERRAFORM_PROVIDER_TEMPLATE_VERSION = get_env("TERRAFORM_PROVIDER_TEMPLATE_VERSION")
}

# ########################################################################################################################
#
# GENERATE PROVIDER FILE
#
# ########################################################################################################################

generate "provider" {
  path = "00_generated_01_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "${local.TERRAFORM_PROVIDER_AWS_VERSION}"
    }

    github = {
      source = "integrations/github"
      version = "${local.TERRAFORM_PROVIDER_GITHUB_VERSION}"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "${local.TERRAFORM_PROVIDER_KUBERNETES_VERSION}"
    }

    archive = {
      source = "hashicorp/archive"
      version = "${local.TERRAFORM_PROVIDER_ARCHIVE_VERSION}"
    }

    template = {
      source = "hashicorp/template"
      version = "${local.TERRAFORM_PROVIDER_TEMPLATE_VERSION}"
    }
  }
}

EOF
}

